# Heart Disease Prediction 

This project aims to predict the presence of heart disease using various regression models. The dataset used for this project contains various health metrics and a target variable indicating the presence or absence of heart disease.

## Project Structure

- `data_loader.py`: This script contains the function to load the dataset.
- `preprocessing.py`: This script handles data preprocessing tasks.
- `models.py`: This script defines and returns a dictionary of regression models.
- `evaluation.py`: This script contains functions to train and evaluate the models.
- `main.py`: This is the main script to run the entire workflow.

## Usage

1. Ensure the dataset is in the correct directory (dataset/heart_disease.csv).

2. Run the main script:

```bash
python main.py
```

3. The script will load the data, preprocess it, train various models, and evaluate their performance. The results, including accuracy and classification reports, will be printed for each model.

## Dependencies

- pandas
- h5py
- Python 3.6+
- scikit-learn

Install dependencies using:

```bash
pip install pandas scikit-learn
```

## Training

1. Adjust the configuration in model.py according to your needs.
2. Run main.py to start training the models.


## Model Evaluation
Evaluate the model using the test data and returns accuracy and classification report.

## License
Feel free to modify and use the code according to your needs.