#model.py
from sklearn.svm import SVR
from sklearn.tree import DecisionTreeRegressor
from sklearn.neural_network import MLPRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.linear_model import LinearRegression, Ridge

def get_models():
    models = {
        "RandomForestRegressor": RandomForestRegressor(),
        "LinearRegression": LinearRegression(),
        "Ridge": Ridge(),
        "SVR": SVR(),
        "DecisionTreeRegressor": DecisionTreeRegressor(),
        "KNeighborsRegressor": KNeighborsRegressor(),
        "MLPRegressor": MLPRegressor(max_iter=500)
    }
    return models

def train_model(model, x_train, y_train):
    model.fit(x_train, y_train)
    return model