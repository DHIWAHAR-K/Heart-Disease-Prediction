#main.py
from data_loader import load_data
from evaluation import evaluate_model
from preprocessing import preprocess_data
from model import get_models, train_model

# Load the data
data = load_data("./dataset/heart_disease.csv")

# Preprocess the data
x_train, x_test, y_train, y_test = preprocess_data(data)

# Get all models
models = get_models()

# Train and evaluate each model
for model_name, model in models.items():
    print(f"Training and evaluating {model_name}...")
    trained_model = train_model(model, x_train, y_train)
    accuracy, report = evaluate_model(trained_model, x_test, y_test)
    print(f"Model: {model_name}")
    print(f"Accuracy: {accuracy}")
    print("Classification Report:")
    print(report)
    print("="*60)